# Flips API v2

## Requirements

-   make
-   docker
    -   docker-compose
-   ansible
-   node

-   The vault password

## Deploy to Production

**ssh config**

```
Host api
  User flips
  Hostname 35.181.50.68
  Port 22
  IdentityFile ~/.ssh/flips_rsa
  IdentitiesOnly yes
```

### Quick change

This will not re-install the node_modules, so make sure that there are no changes in the package\*.json files

**commands**

```
ssh api
cd api_v2/
docker-compose stop
git fetch --all
git checkout master
git rebase origin/master
make build up
```

### Full deploy

This stops all containers, deletes the node_modules, the build directory, and re-installs everything, this is safer

```
ssh api
cd api_v2/
make down
git fetch --all
git checkout master
git rebase origin/master
make prod
```

## Get Started

In order to start developping on the project, perform the following steps:

-   Clone the repository

```
git@gitlab.com:flips-app/flips-api-v2.git
cd flips-api-v2
```

-   Setup the local development environment. You will have to set up your env file.

`cp infrastructure/.env.example infrastructure/.env.dev`

Then fill in the values for your laptop  
**Try not to change too many variables, USER should be enough**

`cp infrastructure/hosts.example infrastructure/hosts`  
`cp ansible.cfg.example ansible.cfg`

-   Install node dependencies

`npm install`

**We use docker for development but it is easier to install the modules outside the container and simply mount them, this is why.**

-   Run the docker

The code is ran using docker containers, it will all be setup when you run the following command :

`make dev`

-   The service is exposed on port 4000 by default and can be accessed through the local url localhost:4000

### Useful commands

-   Retrieve the logs of the API

`make logs`

## Design Rules of API

1. **Make specific updates** (ex: no event update that update all fields)
2. **Don't use tuples** (price: [0,15]) but objects (price: {min: 0, max 15})
3. **ref other collection with Collection['_id']** in Typescript types (ex: type EventDocument { createdBy: UserDocument['_id]})
4. **Mutations naming convention** (model)(Action)(Specific) (ex: userCreate or eventUpdateProfil)
