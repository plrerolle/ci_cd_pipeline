COMPOSE_FILE_PATH := -f docker-compose
API_SERVICE := api
COMPOSE_ARGS := -d
CHECK_MODE := -vvv
PASS_FILE := .vault_pass
ENV_FILE := .env.dev

check-dev: CHECK_MODE=--check
check-dev: dev

check-test: CHECK_MODE=--check
check-test: test

check-serve: CHECK_MODE=--check
check-serve: serve

dev: ENV_FILE=.env.dev
dev: ./infrastructure/${PASS_FILE}
dev: up

prod: serve

serve: PASS_FILE=.prod_vault_pass
serve: ENV_FILE=.env
serve: ./infrastructure/${PASS_FILE}
serve: up

test: ENV_FILE=.env.test
test: up

up:
	@./infrastructure/eval.sh $(ENV_FILE) ./infrastructure/serve.yml $(CHECK_MODE)


create:
ifeq (,$(wildcard ./infrastructure/.prod_vault_pass))
	@echo "You are not admin"
else
	@./infrastructure/eval.sh .env ./infrastructure/create-instance.yml $(CHECK_MODE)
endif

teardown:
ifeq (,$(wildcard ./infrastructure/.prod_vault_pass))
	@echo "You are not admin"
else
	@./infrastructure/eval.sh .env ./infrastructure/teardown.yml $(CHECK_MODE)
endif

requirements:
ifeq (,$(wildcard ./infrastructure/.prod_vault_pass))
	@echo "You are not admin"
else
	@ansible-galaxy install -r ./infrastructure/requirements.yml $(CHECK_MODE)
	@./infrastructure/eval.sh .env ./infrastructure/install-requirements.yml $(CHECK_MODE)

endif

deploy:
ifeq (,$(wildcard ./infrastructure/.prod_vault_pass))
	@echo "You are not admin"
else
	@./infrastructure/eval.sh .env ./infrastructure/deploy.yml $(CHECK_MODE)
endif

encrypt:
ifeq (,$(wildcard ./infrastructure/.vault_pass))
	@echo "You are not dev"
else
	@ansible-vault encrypt --encrypt-vault-id dev ./infrastructure/secrets/development.yml ./infrastructure/secrets/aws.yml ./infrastructure/gce_vars/auth
endif
ifeq (,$(wildcard ./infrastructure/.prod_vault_pass))
	@echo "You are not admin"
else
	@ansible-vault encrypt --encrypt-vault-id prod ./infrastructure/secrets/rsa.yml ./infrastructure/secrets/production.yml 
endif


decrypt:
ifeq (,$(wildcard ./infrastructure/.vault_pass))
	@echo "You are not dev"
else
	@ansible-vault decrypt ./infrastructure/secrets/development.yml ./infrastructure/secrets/aws.yml ./infrastructure/gce_vars/auth
endif
ifeq (,$(wildcard ./infrastructure/.prod_vault_pass))
	@echo "You are not admin"
else
	@ansible-vault decrypt ./infrastructure/secrets/rsa.yml ./infrastructure/secrets/production.yml 
endif

test-logs: COMPOSE_FILE_PATH= -f docker-compose-test
test-logs: API_SERVICE=apitest
test-logs: logs

test-down: COMPOSE_FILE_PATH= -f docker-compose-test
test-down: API_SERVICE=apitest
test-down: down


logs:
	@docker-compose $(COMPOSE_FILE_PATH).yml logs -f $(API_SERVICE)

down:
	@docker-compose $(COMPOSE_FILE_PATH).yml down -v
	@make clean

clean:
	@docker system prune -f
	@docker volume prune -f